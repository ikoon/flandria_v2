/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      //alert("I'm alive!");
      $('#lightgallery').lightGallery();

      $('.grid').imagesLoaded(function () {
        $('.grid').masonry({
          itemSelector: '.grid-item',
          percentPosition: false,
          gutter: 10,
          fitWidth: true
        });
      });
    }
  };

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.cookieSettings = {
    attach: function (context, settings) {
      $('span[data-action="cookie_settings"]', context).click(function() {
        $('.eu-cookie-withdraw-tab').click();
      });
      $(function(){
        $('#sliding-popup').css({bottom: - $('#sliding-popup').height()});
      });
    }
  };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.slides = {
        attach: function (context, settings) {
            $(context).find('.field-name-field-slides.swiper-container').once('ifSlider').each(function (){
                var slides = new Swiper (this, {
                    autoplay: {
                      delay: 6000
                    },
                    speed: 1200,
                    loop: true,
                    effect: 'slide',
                    navigation: {
                      nextEl: '.swiper-button-next',
                      prevEl: '.swiper-button-prev'
                   }
                });
            });

          $(context).find('.module-swiper.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: true,
              loop: true,
              effect: 'slide'
            });
          });

          $(context).find('.showcase-swiper.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: false,
              loop: true,
              slidesPerView:'auto',
              effect: 'slide',
              navigation: {
                nextEl: '.swiper-button-next.showcase',
                prevEl: '.swiper-button-prev.showcase'
              }
            });
          });

          $(context).find('.slider-lightbox.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: false,
              loop: true,
              slidesPerView:'auto',
              effect: 'slide',
              spaceBetween: 3,
              navigation: {
                nextEl: '.swiper-button-next.lightslide',
                prevEl: '.swiper-button-prev.lightslide'
              }
            });
          });

          $(context).find('.slider-lightbox-stock.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: false,
              loop: true,
              slidesPerView:'auto',
              effect: 'slide',
              spaceBetween: 3,
              navigation: {
                nextEl: $(this).parent().find('.swiper-button-next.lightslidestock'),
                prevEl: $(this).parent().find('.swiper-button-prev.lightslidestock')
              }
            });
          });

          $(context).find('.brandslide.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: false,
              loop: true,
              slidesPerView: 4,
              effect: 'slide',
              spaceBetween: 90,
              navigation: {
                nextEl: '.swiper-button-next.brandbutton',
                prevEl: '.swiper-button-prev.brandbutton'
              },
              breakpoints: {
                // when window width is >= 320px
                767: {
                  slidesPerView: 1,
                  spaceBetween: 90
                },
                768: {
                  slidesPerView: 2,
                  spaceBetween: 90
                },
                // when window width is >= 480px
                1024: {
                  slidesPerView: 3,
                  spaceBetween: 90
                },
                // when window width is >= 640px
                1200: {
                  slidesPerView: 4,
                  spaceBetween: 90
                }
              }
            });
          });
        }
    };

    /**
     * offCanvas behaviors
     */
    Drupal.behaviors.offCanvas = {
        attach: function (context, settings) {
            $(context).find('#offCanvasRightOverlap').once('ifRightCanvas').each(function () {
                $menuButton = $('#menuButton');
                $(this).on("opened.zf.offcanvas", function(e){
                    $menuButton.addClass('is-active');
                });
                $(this).on("closed.zf.offcanvas", function(e){
                    $menuButton.removeClass('is-active');
                });
            });
        }
    };

})(jQuery, Drupal);
